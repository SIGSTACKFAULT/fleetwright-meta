## Buff

**Possible Buff Targets:**

- Ships:
  - `acceleration`
  - `delta_v`
  - `ecm` (the actual ECM stat)
  - `energy`
  - `heat`
  - `heal_crew`
  - `raw_ecm` (ECM Volume)
  - `repair`
  - `sensors` (Sensors volume)
  - `wia_ratio` (remember to clamp to [0,1])
- Factions:
  - `corruption`
  - `dissent_target`
  - `industry`
  - `legitimacy`
  - `tax`

## Converter

Powerplants are secretly just converters which take no input.

```yaml
fission:
  density: 5
  durability: 24
  modules:
    converter:
      output:
        ENERGY: 0.08333
        HEAT: 1
```

ISRU stuff would be converters too but I haven't bothered to implement any yet

## Engine

Fuel values are MathJS expressions; `v` is the volume of the component.
Soon(TM) i'll make thrust the same.

```yaml
fusion_drive:
  # ...
  modules:
    engine:
      fuel:
        d3he: 0.001v
        inert_fluid: 1v
      thrust: 300
      works_in_atmo: false
```
