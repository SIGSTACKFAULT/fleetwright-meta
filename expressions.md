Description of the contexts for the various MathJS expressions

[MathJS expression syntax](https://mathjs.org/docs/expressions/syntax.html)

## Combat Station Requirements

- `total`: total number of crew
- `crew_fraction`: fraction of total crew assigned to this station
- `morale`: ship's morale
- specific crew types:
  - `crew.biological`
  - `crew.construct`
  - `crew.infomorph`
  - `crew.mechanical`
  - `crew.brainwashed`

## Government Requirements

- ideology
  - `S`: Supportiveness
  - `I`: Inclusivity
  - `E`: Egalitarianism
  - `P`: Progressivism

## Policy Requirements

same as government requirements
